package com.example.demo.controller;

import com.example.demo.model.User;
import com.example.demo.model.UserDataView;
import com.example.demo.model.UserSave;
import com.example.demo.service.IUserService;
import java.util.List;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.server.ResponseStatusException;

@RestController
@CrossOrigin
public class UserController {
    @Autowired
    private IUserService UserService;

    @PostMapping("/user")
    public User registerUser(@RequestBody UserSave user) {
        try{
            User newUser = UserService.setUser(user);
            if(newUser.getUserId() == 0){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Something wrong when save the data");
            }else{
                return newUser;
            }
        }catch(Exception e){
            throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @PostMapping("/login")
    public String login(@RequestBody UserSave user) {
        try{
            String token = UserService.login(user.getUsername(), user.getPassword());
            return token;
        }catch(Exception e){
            throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @PostMapping("/user/profile")
    public UserDataView getUser(@RequestHeader String token) {
        if(UserService.checkToken(token) == false) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Token not exists");
        }
        try{
            UserDataView user = UserService.getUserData(token);
            if(user.getEmail().equals(null)){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Something wrong when save the data");
            }else{
                return user;
            }
        }catch(Exception e){
            throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @PostMapping("/user/update")
    public User userUpdate(@RequestHeader String token, @RequestBody UserSave userData) {
        if(UserService.checkToken(token) == false) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Token not exists");
        }
        try{
            User user = UserService.updateUserData(token, userData);
            if(user.getEmail().equals(null)){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Something wrong when update the data");
            }else{
                return user;
            }
        }catch(Exception e){
            throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }
}
