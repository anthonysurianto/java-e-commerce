package com.example.demo.controller;

import com.example.demo.model.Products;
import com.example.demo.service.IProductsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.server.ResponseStatusException;

@RestController
@CrossOrigin
public class ProductsController {
    @Autowired
    private IProductsService ProductsService;

    @GetMapping("/products/{id}")
    public Products getProducts(@PathVariable Long id) {

        var products = ProductsService.getById(id);
        if(products.isPresent()){
            return (Products) products.get();
        }else{
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, "Empty");
        }
    }
    
}
