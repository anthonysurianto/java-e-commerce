package com.example.demo.repository;

import com.example.demo.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    // List<User> findByEmail(String Email);
    List<User> findByEmailOrMobileNumber(String email, String MobileNumber);
    List<User> findByToken(String token);
}
