package com.example.demo.model;

import java.util.Date;

public class UserSave {
    public String email;
    public String username;
    public String mobileNumber;
    public String password;
    public String firstName;
    public String lastName;
    public Boolean gender;
    public Date dob;

    
    public UserSave() {
    }

    public UserSave(
        String username,
        String email,
        String mobileNumber,
        String password,
        String firstName,
        String lastName,
        Boolean gender,
        Date dob
        ) {
            this.username = username;
            this.email = email;
            this.mobileNumber = mobileNumber;
            this.password = password;
            this.firstName = firstName;
            this.lastName = lastName;
            this.gender = gender;
            this.dob = dob;
        }

        public String getUsername() {
            return username;
        }
    
        public void setUsername(String username) {
            this.username = username;
        }
    
        public String getEmail() {
            return email;
        }
    
        public void setEmail(String email) {
            this.email = email;
        }
    
        
        public String getMobileNumber() {
            return mobileNumber;
        }
        public void setMobileNumber(String mobileNumber) {
            this.mobileNumber = mobileNumber;
        }
    

        public void setPassword(String password) {
            this.password = password;
        }

        public String getPassword() {
            return password;
        }
    
        
        public String getFirstName() {
            return firstName;
        }
    
        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }
    
        
        public String getLastName() {
            return lastName;
        }
    
        public void setLastName(String lastName) {
            this.lastName = lastName;
        }
    
        
        public Boolean getGender() {
            return gender;
        }
    
        public void setGender(Boolean gender) {
            this.gender = gender;
        }
    
        public Date getDob() {
            return dob;
        }
    
        public void setDob(Date dob) {
            this.dob = dob;
        }
}
