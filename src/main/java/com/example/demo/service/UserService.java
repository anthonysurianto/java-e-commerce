package com.example.demo.service;

import com.example.demo.model.User;
import com.example.demo.model.UserSave;
import com.example.demo.model.UserDataView;
import com.example.demo.repository.UserRepository;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.time.LocalDate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class UserService implements IUserService  {
    @Autowired
    private UserRepository repository;

    public String hashString(String data){
        try {
            MessageDigest messageDigest=MessageDigest.getInstance("MD5");
            messageDigest.update(data.getBytes());
            byte[] digest=messageDigest.digest();
            StringBuffer sb = new StringBuffer();
            for (byte b : digest) {
                sb.append(Integer.toHexString((int) (b & 0xff)));
            }
            return sb.toString();
        }catch (Exception e) {
            return "";
        }
    }

    @Override
    public User setUser(UserSave user) {
        User saveUserData = new User();
        try {          
            saveUserData.setEmail(user.getEmail());
            saveUserData.setFirstName(user.getFirstName());
            saveUserData.setLastName(user.getLastName());
            saveUserData.setMobileNumber(user.getMobileNumber());
            saveUserData.setPassword(hashString(user.getPassword()));
            saveUserData.setGender(user.getGender());
            saveUserData.setDob(user.getDob());

            User saveUser = repository.save(saveUserData);  
            return saveUser;
        }
        catch (Exception e) {
            return saveUserData;
        }
    }

    @Override
    public User updateUserData(String token, UserSave user) {
        List<User> listUser = repository.findByToken(token);
        User updateUserData = new User();   
        if(listUser.size() > 0){
            updateUserData = listUser.get(0);
            updateUserData.setFirstName(user.getFirstName());
            updateUserData.setLastName(user.getLastName());
            updateUserData.setGender(user.getGender());
            updateUserData.setDob(user.getDob());

            User saveUser = repository.save(updateUserData);
            return saveUser;
        }
        return updateUserData;
    }

    @Override
    public Boolean checkToken(String token) {
        List<User> saveUser = repository.findByToken(token);
        if(saveUser.size() > 0){
            return true;
        }
        else return false;
    }

    @Override
    public UserDataView getUserData(String token) {
        User user = new User();
        UserDataView userData = new UserDataView();
        List<User> saveUser = repository.findByToken(token);
        if(saveUser.size() > 0){
            user = saveUser.get(0);
            userData.setEmail(user.getEmail());
            userData.setFirstName(user.getFirstName());
            userData.setLastName(user.getLastName());
            userData.setMobileNumber(user.getMobileNumber());
            userData.setDob(user.getDob());
            userData.setGender(user.getGender());
            return userData;
        }
        else return userData;
    }



    @Override
    public String login(String username, String password) {
        String token = "";
        // return token;
        try {          
            List<User> saveUser = repository.findByEmailOrMobileNumber(username, username);
            if(saveUser.size() > 0){
                User user = saveUser.get(0);
                String hashPass = hashString(password);
                if(user.getPassword().equals(hashPass)){
                    token = hashString(LocalDate.now().toString() + username);
                    user.setToken(token);
                    repository.save(user); 
                }
                return token;
            }
            else return token;
        }
        catch (Exception e) {
            return token;
        }
    }
}
