package com.example.demo.service;

import com.example.demo.model.Products;
import java.util.List;
import java.util.Optional;

public interface IProductsService {

    List<Products> getAll();
    Optional<Products> getById(Long id);
}
