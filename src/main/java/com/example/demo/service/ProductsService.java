package com.example.demo.service;

import com.example.demo.model.Products;
import com.example.demo.repository.ProductsRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class ProductsService implements IProductsService {
    @Autowired
    private ProductsRepository repository;

    @Override
    public List<Products> getAll() {
        return (List<Products>) repository.findAll();
    }

    @Override
    public Optional<Products> getById(Long id) {
        return repository.findById(id);
    }
}
