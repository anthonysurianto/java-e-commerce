package com.example.demo.service;

import com.example.demo.model.User;
import com.example.demo.model.UserDataView;
import com.example.demo.model.UserSave;
import java.util.List;
import java.util.Optional;

public interface IUserService {
    User setUser(UserSave user);
    User updateUserData(String token, UserSave user);
    String login(String username, String password);
    Boolean checkToken(String token);
    UserDataView getUserData(String token);
}
